package org.test.api.controller;
;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.test.domain.projection.ResumeCnab;
import org.test.domain.service.CnabService;
import org.test.repository.filter.CnabFilter;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

class CnabControllerTest {
    @Mock
    CnabService cnabService;

    @InjectMocks
    CnabController cnabController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testSave() {
        MockMultipartFile file = new MockMultipartFile("file", "test-file.txt", "text/plain", "3201903010000014200096206760174753****3153153453JOÃO MACEDO   BAR DO JOÃO".getBytes());

        ResponseEntity<String> result = cnabController.save(file);
        Assertions.assertEquals("Arquivo enviado e processado com sucesso.", result.getBody());
    }

    @Test
    void testSearchCnab() {
        CnabFilter cnabFilter = new CnabFilter();

        List<ResumeCnab> mockCnabList = Arrays.asList(new ResumeCnab(), new ResumeCnab());
        when(cnabService.searchCnab(cnabFilter)).thenReturn(mockCnabList);

        List<ResumeCnab> result = cnabController.searchCnab(cnabFilter);
        Assertions.assertEquals(2, result.size());
    }
}