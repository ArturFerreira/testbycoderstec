package org.test.domain.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;
import org.test.domain.projection.ResumeCnab;
import org.test.repository.CnabRepository;
import org.test.repository.filter.CnabFilter;

import java.io.IOException;
import java.util.List;

class CnabServiceTest {
    @Mock
    CnabRepository cnabRepository;
    @InjectMocks
    CnabService cnabService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testSaveAll() throws IOException {
        MockMultipartFile file = new MockMultipartFile("file", "test-file.txt", "text/plain", "5201903010000013200556418150633123****7687145607MARIA JOSEFINALOJA DO Ó - MATRIZ".getBytes());
        cnabService.saveAll(file);
    }

    @Test
    void testSearchCnab() {
        List<ResumeCnab> result = cnabService.searchCnab(new CnabFilter());
        Assertions.assertEquals(0, result.size());
    }
}