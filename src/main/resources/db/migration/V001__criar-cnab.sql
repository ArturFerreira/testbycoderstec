CREATE table cnab(
  codigo bigint identity(1,1),
  tipo int,
  data date,
  valor numeric(16,4),
  cpf varchar(20),
  cartao varchar(50),
  hora time,
  dono varchar(100),
  loja varchar(100),
  primary key (codigo),
)