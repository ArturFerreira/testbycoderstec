package org.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteByCodersTecApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(TesteByCodersTecApiApplication.class, args);
    }
}