package org.test.api.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.test.domain.projection.ResumeCnab;
import org.test.domain.service.CnabService;
import org.test.repository.filter.CnabFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cnabs")

public class CnabController {

    @Autowired
    private CnabService cnabService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> save(@RequestPart("file") MultipartFile file) {
        try {
            cnabService.saveAll(file);

            return ResponseEntity.ok("Arquivo enviado e processado com sucesso.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Erro ao processar o arquivo: " + e.getMessage());
        }

    }

    @GetMapping(params = "resume")
    public List<ResumeCnab> searchCnab(CnabFilter cnabFilter){
        return cnabService.searchCnab(cnabFilter);
    }


}
