package org.test.domain.projection;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ResumeCnab {
    private String loja;
    private BigDecimal total;

    public ResumeCnab(ResumeCnabDto resumeCnabDTO) {
        this.loja = resumeCnabDTO.getLoja();
        this.total = resumeCnabDTO.getTotal();
    }

    public ResumeCnab() {

    }
}
