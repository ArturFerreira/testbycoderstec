package org.test.domain.projection;

import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;

public interface ResumeCnabDto {
    @Value("#{target['loja']}")
    String getLoja();

    @Value("#{target['total']}")
    BigDecimal getTotal();
}
