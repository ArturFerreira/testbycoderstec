package org.test.domain.model;

public enum Tipo {
    Debito,
    Boleto,
    Financiamento,
    Credito,
    Recebimento_Emprestimo,
    Vendas,
    Recebimento_Ted,
    Recebimento_Doc,
    Aluguel
}
