package org.test.domain.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Time;
import java.util.Date;

@Data
@Entity
public class Cnab {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long codigo;

    private Tipo tipo;

    private Date data;

    private Double valor;

    private String cpf;

    private String cartao;

    private Time hora;

    private String dono;

    private String loja;

}
