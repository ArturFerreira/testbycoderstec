package org.test.domain.service;

import org.springframework.web.multipart.MultipartFile;
import org.test.domain.model.Cnab;
import org.test.domain.model.Tipo;
import org.test.domain.projection.ResumeCnab;
import org.test.domain.projection.ResumeCnabDto;
import org.test.repository.CnabRepository;
import org.test.repository.filter.CnabFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CnabService {

    @Autowired
    private CnabRepository cnabRepository;

    @Transactional
    public void saveAll(MultipartFile file) throws IOException {
        List<Cnab> cnabList = processFile(file);

        cnabRepository.saveAll(cnabList);
    }
    public List<ResumeCnab> searchCnab(CnabFilter cnabFilter) {
        List<ResumeCnabDto> result;

        if (cnabFilter.getLoja() == null) {
            result = cnabRepository.searchCnabAll();
        } else {
            result = cnabRepository.searchCnab(cnabFilter);
        }

        return result.stream()
                .map(this::converterResumeCnab)
                .collect(Collectors.toList());
    }

    private ResumeCnab converterResumeCnab(ResumeCnabDto resumeCnabDto) {
        return new ResumeCnab(resumeCnabDto);
    }

    private List<Cnab> processFile(MultipartFile file) throws IOException {
        List<Cnab> cnabList = new ArrayList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));

        String line;
        while ((line = br.readLine()) != null) {
            Cnab cnab = new Cnab();

            String tipoStr = line.substring(0, 1);
            cnab.setTipo(Tipo.Financiamento);

            String dataStr = line.substring(1, 9);
            cnab.setData(parseData(dataStr));

            String valorStr = line.substring(9, 19);
            cnab.setValor(Double.parseDouble(valorStr) / 100);

            String cpfStr = line.substring(19, 30);
            cnab.setCpf(cpfStr);

            String cartaoStr = line.substring(30, 42);
            cnab.setCartao(cartaoStr);

            String horaStr = line.substring(42, 48);
            cnab.setHora(parseHora(horaStr));

            String donoStr = line.substring(48, 62);
            cnab.setDono(donoStr);

            String lojaStr = line.substring(62, 80);
            cnab.setLoja(lojaStr);

            cnabList.add(cnab);
        }

        return cnabList;
    }

    private Date parseData(String dateStr) {
        SimpleDateFormat formatoEntrada = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formatoSaida = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date data = formatoEntrada.parse(dateStr);
            String dataFormatada = formatoSaida.format(data);
            return formatoSaida.parse(dataFormatada);
        } catch (ParseException e) {
            return null;
        }
    }

    private Time parseHora(String timeStr) {
        SimpleDateFormat formatoEntrada = new SimpleDateFormat("HHmmss");

        try {
            Date date = formatoEntrada.parse(timeStr);
            Time time = new Time(date.getTime());
            return time;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

}
