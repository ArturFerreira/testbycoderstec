package org.test.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.test.domain.model.Cnab;
import org.springframework.data.jpa.repository.JpaRepository;
import org.test.domain.projection.ResumeCnabDto;
import org.test.repository.filter.CnabFilter;

import java.util.List;

public interface CnabRepository extends JpaRepository<Cnab, Long>{
    @Query(value = "select loja, sum(valor) as total from cnab a " +
            "group by loja order by loja", nativeQuery = true)
    List<ResumeCnabDto> searchCnabAll();
    @Query(value = "select loja, sum(valor) as total from cnab a " +
            "where loja like %:#{#filter.loja}%  group by loja order by loja", nativeQuery = true)
    List<ResumeCnabDto> searchCnab(@Param("filter") CnabFilter filter);
}
